package info.hccis.performancehall_mobileappbasicactivity.entity;

/*
 * @Author: BJM (Modified from Mariana Alkabalan (Flower Shop App)
 * @since 20220314
 * Reference: https://learntodroid.com/how-to-send-json-data-in-a-post-request-in-android/
 */

import info.hccis.performancehall_mobileappbasicactivity.api.ApiWatcher;
import info.hccis.performancehall_mobileappbasicactivity.api.JsonTicketOrderApi;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class TicketOrderRepository {

    private static TicketOrderRepository instance;

    private JsonTicketOrderApi jsonTicketOrderApi;

    public static TicketOrderRepository getInstance() {
        if (instance == null) {
            instance = new TicketOrderRepository();
        }
        return instance;
    }

    public TicketOrderRepository() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiWatcher.API_BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonTicketOrderApi = retrofit.create(JsonTicketOrderApi.class);
    }

    public JsonTicketOrderApi getTicketOrderService() {
        return jsonTicketOrderApi;
    }

}
