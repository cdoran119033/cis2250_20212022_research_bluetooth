package info.hccis.performancehall_mobileappbasicactivity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import info.hccis.performancehall_mobileappbasicactivity.entity.TicketOrder;
import info.hccis.performancehall_mobileappbasicactivity.entity.TicketOrderRepository;
import info.hccis.performancehall_mobileappbasicactivity.entity.TicketOrderViewModel;
import info.hccis.performancehall_mobileappbasicactivity.databinding.FragmentAddOrderBinding;
import info.hccis.performancehall_mobileappbasicactivity.util.ContentProviderUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * AddOrderFragment
 * This Java class is associated with the add order fragment.  Note it works with the controls defined
 * on the res/layout/fragment_add_order layout file
 *
 * @author BJM/CIS2250
 * @since 20220129
 */

public class AddOrderFragment extends Fragment {

    public static final String KEY = "info.hccis.performancehall.ORDER";
    private FragmentAddOrderBinding binding;
    private TicketOrder ticketOrder;
    private TicketOrderViewModel ticketOrderViewModel;
    private TicketOrderRepository ticketOrderRepository;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("AddOrderFragment BJM", "onCreate triggered");
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        Log.d("AddOrderFragment BJM", "onCreateView triggered");
        binding = FragmentAddOrderBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }


    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("AddOrderFragment BJM", "onViewCreated triggered");

        ticketOrderViewModel = new ViewModelProvider(getActivity()).get(TicketOrderViewModel.class);

        //******************************************************************************
        // Content Providers
        // Check permission for user to use the calendar provider.
        //******************************************************************************

        final int callbackId = 42;
        ContentProviderUtil.checkPermission(getActivity(), callbackId, Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR);

        ticketOrderRepository = ticketOrderRepository.getInstance();
        //************************************************************************************
        // Add a listener on the button.
        //************************************************************************************

        binding.buttonCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("AddOrderFragment BJM", "Calculate was clicked");

                try {
                    //************************************************************************************
                    // Call the calculate method
                    //************************************************************************************

                    calculate();

                    //************************************************************************************
                    // I have made the TicketOrderBO implement Serializable.  This will allow us to
                    // serialize the object and then it can be passed in the bundle.  Note that the
                    // calculate method also sets the
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(KEY, ticketOrder);

                    //************************************************************************************
                    // See the view model research component.  The ViewModel is associated with the Activity
                    // and the add and view fragments share the same activity.  Am using the view model
                    // to hold the list of TicketOrderBO objects.  This will allow both fragments to
                    // access the list.
                    //************************************************************************************

                    //******************************************************************************
                    // Handle the new ticket order
                    // Use the post service to add the new ticket to the web database.
                    //******************************************************************************

                    //ticketOrderViewModel.getTicketOrders().add(ticketOrder);
                    ticketOrderRepository.getTicketOrderService().createTicketOrder(ticketOrder).enqueue(new Callback<TicketOrder>() {
                        @Override
                        public void onResponse(Call<TicketOrder> call, Response<TicketOrder> r) {
                            Toast.makeText(getContext(), "Ticket Order is successfully added!", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<TicketOrder> call, Throwable t) {
                            Toast.makeText(getContext(), "Error Adding Ticket Order: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });


                    //******************************************************************************
                    //Activity for Thursday.
                    //Call the post rest api web service to have the new ticket order added to the database.
                    //******************************************************************************

                    Snackbar.make(view, "Have added a ticket.  Want to add this new ticket to the database. " +
                            "How should we handle this?", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                    //******************************************************************************
                    //Create the event without using Calendar intent
                    //******************************************************************************

                    long eventID = ContentProviderUtil.createEvent(getActivity(), ticketOrder.toString());
                    Toast.makeText(getActivity(), "Calendar Event Created (" + eventID + ")", Toast.LENGTH_SHORT);
                    Log.d("BJM Calendar", "Calendar Event Created (" + eventID + ")");

                    //Using an intent
//                    ContentProviderUtil.createCalendarEventIntent(getActivity(), ticketOrder.toString());


                    //******************************************************************************
                    // Send a broadcast.
                    //******************************************************************************

                    //Send a broadcast.
                    LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(view.getContext());
                    Intent intent = new Intent();
                    intent.setAction("info.hccis.phall.order");
                    lbm.sendBroadcast(intent);
                    getActivity().sendBroadcast(intent);


                    //************************************************************************************
                    // Navigate to the view orders fragment.  Note that the navigation is defined in
                    // the nav_graph.xml file.
                    //************************************************************************************

                    NavHostFragment.findNavController(AddOrderFragment.this)
                            .navigate(R.id.action_AddOrderFragment_to_ViewOrdersFragment, bundle);
                } catch (Exception e) {
                    //************************************************************************************
                    // If there was an exception thrown in the calculate method, just put a message
                    // in the Logcat and leave the user on the add fragment.
                    //************************************************************************************
                    Log.d("AddOrderFragment BJM", "Error calculating: " + e.getMessage());
                }
            }
        });

    }

    /**
     * calculate the ticket cost based on the controls on the view.
     *
     * @throws Exception Throw exception if number of tickets entered caused an issue.
     * @author CIS2250
     * @since 20220118
     */
    public void calculate() throws Exception {

        Log.d("BJM-MainActivity", "HollPass Number entered =" + binding.editTextHollpassNumber.getText().toString());
        Log.d("BJM-MainActivity", "Number of tickets = " + binding.editTextNumberOfTickets.getText().toString());
        Log.d("BJM-MainActivity", "Calculate button was clicked.");

        int hollPassNumber = 0;
        try {
            hollPassNumber = Integer.parseInt(binding.editTextHollpassNumber.getText().toString());
        } catch (Exception e) {
            hollPassNumber = 0;
        }
        boolean validHollPassNumber;
        int numberOfTickets;
        try {
            numberOfTickets = Integer.parseInt(binding.editTextNumberOfTickets.getText().toString());
        } catch (Exception e) {
            numberOfTickets = 0;
        }
        ticketOrder = new TicketOrder();
        ticketOrder.setHollpassNumber(hollPassNumber);
        ticketOrder.setNumberOfTickets(numberOfTickets);

        //************************************************************************************
        // Add some validation to ensure that the ticket is in a correct range (1-20 in this case.
        //************************************************************************************

        try {
            if (!ticketOrder.validateNumberOfTickets()) {
                throw new Exception("Invalid Number of tickets");
            }
            double cost = ticketOrder.calculateTicketPrice();

            //************************************************************************************
            //Now that I have the cost, want to set the value on the textview.
            //************************************************************************************

            Locale locale = new Locale("en", "CA");
            DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(locale);
            DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(locale);
            decimalFormat.setDecimalFormatSymbols(dfs);
            String formattedCost = decimalFormat.format(cost);

            binding.textViewCost.setText(formattedCost);

        } catch (NumberFormatException nfe) {
            //************************************************************************************
            //This isn't possible as the edit text control is set to only accept numbers
            //************************************************************************************

            binding.editTextNumberOfTickets.setText("");
            binding.textViewCost.setText("Invalid number of tickets");
            binding.textViewCost.setError("Unable to calculate");
            throw nfe;
        } catch (Exception e) {
            //************************************************************************************
            // Exception is thrown if the number of tickets isn't in the valid range.
            //************************************************************************************
            binding.editTextNumberOfTickets.setText("");
            binding.textViewCost.setText("Maximum number of tickets is " + TicketOrder.MAX_TICKETS);
            binding.textViewCost.setError("Unable to calculate");
            throw e;
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}