package info.hccis.performancehall_mobileappbasicactivity;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Set;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 *
 * @since 20210303
 * @author mrahman2
 */
public class BluetoothActivity extends AppCompatActivity {

    // Declaring constant variable
    private static final  int REQUEST_ENABLE_BT=0;
    private static final  int REQUEST_DISCOVER_BT=1;

    //Declaring variable
    TextView mTvStatus, mTvPairedDevices;
    ImageView mIvBluetooth;
    Button mBtnOn,mBtnOff,mBtnDiscoverable,mBtnPairedDevices;

    //Bluetooth adapter
    BluetoothAdapter bluetoothAdapter;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        mTvStatus = findViewById(R.id.tvStatusBluetooth);
        mTvPairedDevices = findViewById(R.id.tvPairedDevices);
        mIvBluetooth = findViewById(R.id.ivBluetooth);
        mBtnOn=findViewById(R.id.btnOn);
        mBtnOff=findViewById(R.id.btnOff);
        mBtnDiscoverable=findViewById(R.id.btnDiscoverable);
        mBtnPairedDevices=findViewById(R.id.btnPaired);

        //Check if bluetooth is available or not
        bluetoothAdapter= BluetoothAdapter.getDefaultAdapter();
        if(bluetoothAdapter==null){
            mTvStatus.setText("Bluetooth is not available");
        }else{
            mTvStatus.setText("Bluetooth is available");

            //Display bluetooth on/off icon
        }
        if(bluetoothAdapter.isEnabled()){
            mIvBluetooth.setImageResource(R.drawable.bluetoothon);
            showToast("Bluetooth is enabled.");
        }else{
            mIvBluetooth.setImageResource(R.drawable.bluetoothoff);
            showToast("Bluetooth is not enabled.");
        }

        //Button click Turn on
        mBtnOn.setOnClickListener(v -> {
            if(!bluetoothAdapter.isEnabled()){
                showToast("Turning on Bluetooth....");
                Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(intent,REQUEST_ENABLE_BT);
            }else {
                showToast("Bluetooth is already on.");
            }
        });

        //Button click Turn off
        mBtnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bluetoothAdapter.isEnabled()){
                    bluetoothAdapter.disable();
                    showToast("Turning Bluetooth off...");
                   mIvBluetooth.setImageResource(R.drawable.bluetoothoff);
                }else {
                    showToast("Bluetooth is already off.");
                }
            }
        });

        //Button click Discoverable
        mBtnDiscoverable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!bluetoothAdapter.isDiscovering()){
                    showToast("Making your device discoverable");
                    Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                    startActivityForResult(intent,REQUEST_DISCOVER_BT);
                }
            }
        });

        //Button click Paired Devices
        mBtnPairedDevices.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                if (bluetoothAdapter.isEnabled()) {

                    mTvPairedDevices.setText("Paired Devices");
                    Set<BluetoothDevice> devices = bluetoothAdapter.getBondedDevices();

                    for(BluetoothDevice device :devices){
                        mTvPairedDevices.append(("\n Device : " + device.getName() + " , " + device));
                    }
                }else {
                    showToast("Turn on Bluetooth to get connected devices");
                }
            }
        });
    }

    /**
     * Method to display if bluetooth on or off
     * <p>
     *
     * @since 20210303
     * @author mrahman2
     */
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent date){
        super.onActivityResult(requestCode,resultCode,date);

        switch (requestCode){
            case REQUEST_ENABLE_BT:
                if (requestCode == RESULT_OK){
                    mIvBluetooth.setImageResource(R.drawable.bluetoothon);
                    showToast( "Bluetooth is On");
                }else {
                    showToast("Bluetooth is Off");
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode,date);
    }

    /**
     * Method to display message
     * <p>
     *
     * @since 20210303
     * @author mrahman2
     */
    private void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
