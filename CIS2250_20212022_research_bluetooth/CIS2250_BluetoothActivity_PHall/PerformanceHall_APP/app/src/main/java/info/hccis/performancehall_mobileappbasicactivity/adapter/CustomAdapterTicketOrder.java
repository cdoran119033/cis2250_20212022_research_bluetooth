package info.hccis.performancehall_mobileappbasicactivity.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import info.hccis.performancehall_mobileappbasicactivity.R;
import info.hccis.performancehall_mobileappbasicactivity.entity.TicketOrder;

public class CustomAdapterTicketOrder extends RecyclerView.Adapter<CustomAdapterTicketOrder.TicketOrderViewHolder> {

    private List<TicketOrder> ticketOrderArrayList;

    public CustomAdapterTicketOrder(List<TicketOrder> ticketOrderBOArrayList) {
        this.ticketOrderArrayList = ticketOrderBOArrayList;
    }

    ;

    @NonNull
    @Override
    public TicketOrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_order, parent, false);
        return new TicketOrderViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(@NonNull TicketOrderViewHolder holder, int position) {

        String numberOfTickets = "" + ticketOrderArrayList.get(position).getNumberOfTickets();
        String hollpass = "" + ticketOrderArrayList.get(position).getHollpassNumber();
        String cost = "" + ticketOrderArrayList.get(position).getCostOfTickets();
        holder.textViewNumberOfTickets.setText(numberOfTickets);
        holder.textViewHollPass.setText(hollpass);
        holder.textViewCost.setText(cost);

    }

    @Override
    public int getItemCount() {
        return ticketOrderArrayList.size();
    }

    public class TicketOrderViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewNumberOfTickets;
        private TextView textViewHollPass;
        private TextView textViewCost;

        public TicketOrderViewHolder(final View view) {
            super(view);
            textViewNumberOfTickets = view.findViewById(R.id.textViewNumberOfTicketsListItem);
            textViewHollPass = view.findViewById(R.id.textViewHollpassListItem);
            textViewCost = view.findViewById(R.id.textViewCostListItem);
        }
    }


}
