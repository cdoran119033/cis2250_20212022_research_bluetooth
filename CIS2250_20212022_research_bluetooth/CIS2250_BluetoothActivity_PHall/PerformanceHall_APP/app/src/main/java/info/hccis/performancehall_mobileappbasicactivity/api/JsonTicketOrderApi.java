package info.hccis.performancehall_mobileappbasicactivity.api;
import java.util.List;
import java.util.Map;

import info.hccis.performancehall_mobileappbasicactivity.entity.TicketOrder;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface JsonTicketOrderApi {

    /**
     * This abstract method to be created to allow retrofit to get list of ticket orders
     * @return List of ticket orders
     * @since 20220202
     * @author BJM (with help from the retrofit research!).
     */

    @GET("ticketOrders")
    Call<List<TicketOrder>> getTicketOrders();
    @POST("ticketOrders")
    Call<TicketOrder> createTicketOrder(@Body TicketOrder ticketOrder);

}